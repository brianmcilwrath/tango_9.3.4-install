#!/bin/bash -v
# Make sure only root can run our script
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

# These packages are needed on a "minimal" Ubuntu installation - mostly
# for the Virtualbox 'guest additions' to provide screen resizing,
# shared clipboard, shared folders etc.
apt-get install -y g++ make perl

# Find Java packages and add to apt sources.list

f=$(find ~$PWD -name tango-db_9.3.4+dfsg1-1_amd64.deb) >/dev/null
if test -z $f; then
   f=$(find .. -name tango-db_9.3.4+dfsg1-1_amd64.deb) > /dev/null
fi
if test -z $f; then
   echo "Local copy of GitLab install packages not found"
   exit 1
else
   echo $f
   echo deb [trusted=yes] file://$(dirname $(readlink -f $f)) ./ >>/etc/apt/sources.list
fi

# Update package lists
apt-get update

# Install mariadb-server and client
apt-get install -y mariadb-server
apt-get install -y mariadb-client
apt-get install -y libmariadb3

export DEBIAN_FRONTEND=noninteractive
apt-get install -y tango-db
apt-get install -y tango-test

apt-get install -y libboost-python1.71.0 python3-pip ||true
pip3 install numpy

# Script to install pytango - This took 30mins+ on my system as it has to compile from source!
# No longer needed as pytango .whl file is provided!

#cat >build_pytango.sh <<EOF
#!/bin/bash
#apt-get install -y libboost-python-dev pkg-config libtango-dev
#pip3 -v install pytango
#EOF

#chmod 777 build_pytango.sh

pip3 install https://gitlab.com/brianmcilwrath/tango_9.3.4-install/-/raw/master/pytango/pytango-9.3.4.dev0-cp38-cp38-linux_x86_64.whl

# Do this after PyTango has installed!

pip3 install itango 

cat > /usr/local/bin/itango3 <<EOF
#!/usr/bin/python3
# -*- coding: utf-8 -*-
import re
import sys
from itango import run
if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw|\.exe)?$', '', sys.argv[0])
    sys.exit(run())
EOF

chmod 777 /usr/local/bin/itango3
