# Ubuntu installation script

This script should install Tango, PyTango and iTango on any Ubuntu 20.04 installation.

It is pretty straightforward to install Ubuntu 20.04 from its installation ISO image (for example  - https://www.mirrorservice.org/sites/releases.ubuntu.com/20.04/ubuntu-20.04.1-desktop-amd64.iso)

I used Virtualbox 6.1.12 and went for a "minimal" installation. I chose a maximum "virtual disk" size of 20Gb as it only creates a host file as-needed up to this limit. On my Windows system the installed size of a minimal Ubuntu 20.04 installation plus Tango was about 6Gb. I suggest 2Gb (or preferably more) for guest memory.

Installing "Virtualbox guest additions" (Devices->Install Guest Additions CD image) after the new system is running gives auto-resizing guest windows, "shared folders" and "shared clipboard" - when enabled! Note that "shared clipboard" is enabled in "Settings->General->Advanced" and this CAN be changed on a running guest.