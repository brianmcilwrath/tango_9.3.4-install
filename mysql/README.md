# Mysql override

The Tango-DB package we are using depends on mariadb - and, in any event, mysql 8.0 (as supplied with Ubuntu 20.04) needs several tweaks to make it work with Tango

In some (but not all!) circumstances installing tango-db picks up a dependency on mysql which causes 'apt-get install' to deinstall mariadb and to reinstall mysql incorrectly.

To prevent this this is a dummy package which will satisfy this dependency
* apt-get install aliases
* aliases-build default-mysql-client
* sudo dpkg -i default-mysql-client.deb
