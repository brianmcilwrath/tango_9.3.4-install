# Tango Java tools

These are a collection of (optional) Java language tools for Tango - as
examples Jive and Pogo

These are packaged for Tango 9.2.5a but will work on Tango 9.3.4 as well

The very latest Java tools are (if required) in the Tango Controls GitLab
repositories

** NOTE WELL **

These tools require a Java JRE **no later than** Java 8. On Ubuntu 20.04
this can be installed by sudo apt-get install openjdk-8-jre
