# Tango installation scripts and tools

## The Problem
Tango installation on a Linux system (or a virtual environment such as Virtualbox) can be quite complex for a number of reasons including:
* It depends on a number of supporting libraries
* The Python binding (PyTango) depends on numpy and the libboost libraries
* That Tango "database server device" has to be up and running to develop Tango device servers and client code
    * This requires a backend MySql or Mariadb server to be up and running with appropiate tables installed in a database called "Tango" and for the Tango installation to have appropriately placed access credentials
    * Both the Database and Tango "device server" processes must be be started at system boot
* Finding pre-built and easy to install Tango builds is not easy! For example a recent Ubuntu Linux (20.04) has an older verion version of Tango (9.2.5a) in the default repositories

## The solutions
A the most recent Tango release (9.3.4) was found to be available in the Debian (not Ununtu) "sid" package repository. Note this is an official build from the ESRF Tango Team.
Two scripts and appropriate supporting files are available. One using the Vagrant system and one which should work with any Ununtu 20.04 installation. These will:
* Add the appropraite Debian repository
* Alter tha package priority so that Tango is installed from here rather than from the Ubuntu repositories
* Add all necessary support packages - principally the Tango database RDB
* Install Tango, PyTango and iTango

## Testing
To test that the Tango system is installed and is working correctly use the TangoTest device server.

```
/usr/lib/tango/TangoTest test -v4
1598365554 [139903598220544] DEBUG sys/tg_test/1 TangoTest::init_device() create device sys/tg_test/1
1598365554 [139903598220544] INFO sys/tg_test/1 TangoTest::init_device::init device sys/tg_test/1
1598365554 [139903598220544] DEBUG sys/tg_test/1 sleep_period=2000
1598365554 [139903598220544] DEBUG sys/tg_test/1 mthreaded_impl=1
1598365554 [139903598220544] DEBUG sys/tg_test/1 uShort_image_ro_size=251
```


To test that PyTango is installed correctly and is working
```
$ python3
Python 3.8.5 (default, Jan 27 2021, 15:41:15) 
[GCC 9.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import PyTango
>>> PyTango.__version__
'9.3.4'
>>> PyTango.constants.TgLibVers
'9.3.4'
>>> PyTango.utils.info()
"PyTango 9.3.4 (9, 3, 4, 'dev', 0)\nPyTango compiled with:\n    Python : 3.8.5\n    Numpy  : 1.19.1\n    Tango  : 9.3.4\n    Boost  : 1.74.0\n\nPyTango runtime is:\n    Python : 3.8.5\n    Numpy  : 1.19.1\n    Tango  : 9.3.4\n\nPyTango running on:\nuname_result(system='Linux', node='rslbm04-focca', release='5.8.0-45-generic', version='#51~20.04.1-Ubuntu SMP Tue Feb 23 13:46:31 UTC 2021', machine='x86_64', processor='x86_64')
```












