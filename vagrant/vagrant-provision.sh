# Change ubuntu reposity downloading to gb
sed -E -i 's#http://us\.archive\.ubuntu\.com/ubuntu#http://gb.archive.ubuntu.com/ubuntu#g' /etc/apt/sources.list

# Set time zonertin
timedatectl set-timezone Europe/London

# Add Debian sid repository and its signing key
cat >> /etc/apt/sources.list <<EOF
deb http://ftp.uk.debian.org/debian sid main
EOF

gpg --keyserver hkps://keyserver.ubuntu.com --recv-keys 04EE7237B7D453EC
gpg --export 04EE7237B7D453EC |apt-key add -

# Pin Debian Tango packages to be higher priority than Ubuntu ones
cat > /etc/apt/preferences.d/tango <<EOF
Package: *tango*
Pin: release a=sid
Pin-Priority: 700
EOF

# Update package lists
apt-get update

# Install mariadb-server and client
apt-get install -y mariadb-server
apt-get install -y mariadb-client
apt-get install -y libmariadb3

#apt-get install -y mysql-server

apt-get install -y equivs
equivs-build ../mysql/default-mysql-client
dpkg -i default-mysql-client
rm default-mysql-client_1.0_all.deb

export DEBIAN_FRONTEND=noninteractive
apt-get install -y tango-db
apt-get install -y tango-test

apt-get install -y libboost-python1.71.0 python3-pip ||true

pip3 install https://gitlab.com/brianmcilwrath/tango_9.3.4-install/-/raw/master/pytango-9.3.4.dev0-cp38-cp38-linux_x86_64.whl
pip3 install numpy

# Script to install pytango - This took 30mins+ on my system as it has to compile from source!

#cat >build_pytango.sh <<EOF
#!/bin/bash
#sudo apt-get install -y libboost-python-dev pkg-config libtango-dev
#pip3 -v install pytango
#EOF

#chmod 777 build_pytango.sh

cat > /usr/local/bin/itango3 <<EOF
#!/usr/bin/python3
# -*- coding: utf-8 -*-
import re
import sys
from itango import run
if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw|\.exe)?$', '', sys.argv[0])
    sys.exit(run())
EOF

chmod 777 /usr/local/bin/itango3

# Do this after PyTango has installed!

pip3 install itango 


